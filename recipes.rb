# frozen_string_literal: true

require 'sinatra'
require_relative 'services/contentful'

get '/' do
  @recipes = Contentful.fetch_entries('recipe')
  erb :recipes
end

get '/recipes/:id' do
  @recipe = Contentful.fetch_entry(params[:id])
  erb :recipe
end
